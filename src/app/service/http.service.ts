import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { API_KEY } from '../constant/api.constant';
import { PARAM } from '../constant/param.constant';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  private params = new HttpParams()
    .set(PARAM.API_KEY, API_KEY);

  constructor(private http: HttpClient) {
  }

  public get(url) {
    return this.http.get(url, {params: this.params});
  }

  public getWithParam(url, param) {
    return this.http.get(url, {params: param.set(PARAM.API_KEY, API_KEY)});
  }
}
