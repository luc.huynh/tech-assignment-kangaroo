import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { URL } from '../constant/url.constant';
import { PARAM } from '../constant/param.constant';

@Injectable({
  providedIn: 'root'
})
export class ForexService {

  constructor(private httpService: HttpService) {
  }

  public getSymbols(): Observable<any> {
    return this.httpService.get(URL.SYMBOLS_GET);
  }

  public getQuotes(symbol: string): Observable<any> {
    const params = new HttpParams()
      .set(PARAM.PAIRS, symbol);

    return this.httpService.getWithParam(URL.QUOTES, params);
  }

  public getMarketStatus(): Observable<any> {
    return this.httpService.get(URL.MARKET_STATUS_GET);
  }

  public getQuota(): Observable<any> {
    return this.httpService.get(URL.QUOTA_GET);
  }
}
