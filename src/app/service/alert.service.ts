import { Injectable } from '@angular/core';
import swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor() { }

  public alertError(content) {
    const baseOptions = {
      title: 'ERROR!',
      html: content,
      type: 'error',
      timer: 5000,
      heightAuto: false
    };
    return swal((<any>Object).assign(baseOptions)).catch(swal.noop);
  }
}
