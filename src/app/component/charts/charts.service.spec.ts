import { TestBed } from '@angular/core/testing';

import { ChartsService } from './charts.service';

describe('ChartsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be return human hour when input timestamp', () => {
    const service: ChartsService = TestBed.get(ChartsService);
    expect(service.convertTimestamp(1546916005)).toEqual('9:53:25');
  });
});
