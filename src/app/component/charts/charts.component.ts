import { Component, OnDestroy, OnInit } from '@angular/core';
import { ForexService } from '../../service/forex.service';
import { AlertService } from '../../service/alert.service';
import { CHARTS } from '../../constant/charts.constant';
import { COLOR } from '../../constant/color.constant';
import { TIME } from '../../constant/time.constant';
import { MESSAGE } from '../../constant/message.constant';
import { ChartsService } from './charts.service';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit, OnDestroy {

  constructor(public forexService: ForexService,
              public alertService: AlertService,
              public chartsService: ChartsService) {
  }

  public runFirstTime: boolean;
  public quotaRemaining: number;
  public quotaLimit: number;
  public marketIsOpen: boolean;
  public marketStatus: string;
  public symbolTitle: string;
  public interval: any;
  public symbols: string[];

  // options charts
  public view: any[] = [CHARTS.Y_VIEW, CHARTS.X_VIEW];
  public xAxisLabel = CHARTS.X_AXIS_LABEL;
  public yAxisLabel = CHARTS.Y_AXIS_LABEL;
  public colorScheme = {
    domain: [COLOR.PRICE_LINE, COLOR.ASK_LINE, COLOR.BID_LINE]
  };
  public multiLine: any[];

  public initData() {
    this.symbols = [''];
    this.symbolTitle = '';
    this.quotaRemaining = 0;
    this.quotaLimit = 0;
    this.runFirstTime = true;

    this.multiLine = [
      {
        name: CHARTS.PRICE,
        series: [{
          name: 0,
          value: 0
        }]
      },
      {
        name: CHARTS.ASK,
        series: [{
          name: 0,
          value: 0
        }]
      },
      {
        name: CHARTS.BID,
        series: [{
          name: 0,
          value: 0
        }]
      }
    ];
  }

  ngOnInit() {
    this.initData();

    this.loadDataRealTime();
  }

  ngOnDestroy(): void {
    clearInterval(this.interval);
  }

  public loadDataRealTime() {
    this.getQuota();

    this.interval = setInterval(() => {
      if (this.quotaRemaining > 0) {
        this.getQuota();
      }
    }, TIME.TIME_TO_RELOAD);
  }

  public getQuota() {
    this.forexService.getQuota().subscribe(
      (response) => {
        this.quotaLimit = response.quota_limit;
        this.quotaRemaining = response.quota_remaining;

        if (this.quotaRemaining > 0) {
          this.getListSymbols();
          this.getMarketStatus();
        } else {
          this.alertService.alertError(MESSAGE.OUT_OF_QUOTA);
        }
      },
      () => {
        this.alertService.alertError(MESSAGE.SOMETHING_WRONG);
      }
    );
  }

  public getListSymbols() {
    this.forexService.getSymbols().subscribe(
      (response) => {
        if (response.error === true) {
          this.alertService.alertError(MESSAGE.OUT_OF_QUOTA);
        } else {
          this.symbols = response;

          if (this.symbolTitle === '') {
            this.symbolTitle = response[0];
          }

          this.getQuotes(this.symbolTitle);
        }
      },
      () => {
        this.alertService.alertError(MESSAGE.SOMETHING_WRONG);
      }
    );
  }

  public getMarketStatus() {
    this.forexService.getMarketStatus().subscribe(
      (response) => {
        if (response.error === true) {
          this.alertService.alertError(MESSAGE.OUT_OF_QUOTA);
        } else {
          this.marketIsOpen = response.market_is_open;
          this.marketStatus = this.marketIsOpen ? 'OPEN' : 'CLOSE';
        }
      },
      () => {
        this.alertService.alertError(MESSAGE.SOMETHING_WRONG);
      }
    );
  }

  public getQuotes(symbol: string) {
    if (this.runFirstTime && this.multiLine[CHARTS.PRICE_INDEX].series[0].value === 0) {
      this.runFirstTime = false;
      this.multiLine = this.chartsService.clearMultiLine(this.multiLine);
    }

    this.forexService.getQuotes(symbol).subscribe(
      (response) => {
        if (response.error === true) {
          this.alertService.alertError(MESSAGE.OUT_OF_QUOTA);
        } else {
          const formattedTime = this.chartsService.convertTimestamp(response[0].timestamp);

          this.multiLine[CHARTS.PRICE_INDEX].series.push({name: formattedTime, value: response[0].price});
          this.multiLine[CHARTS.ASK_INDEX].series.push({name: formattedTime, value: response[0].ask});
          this.multiLine[CHARTS.BID_INDEX].series.push({name: formattedTime, value: response[0].bid});

          this.multiLine = this.chartsService.checkLimitedTimeOfChart(this.multiLine);

          this.multiLine = [...this.multiLine];
        }
      },
      () => {
        this.alertService.alertError(MESSAGE.SOMETHING_WRONG);
      }
    );
  }

  public onChangeSymbol($event) {
    this.symbolTitle = $event.srcElement.value;
    this.multiLine = this.chartsService.clearMultiLine(this.multiLine);

    clearInterval(this.interval);

    this.loadDataRealTime();
  }
}
