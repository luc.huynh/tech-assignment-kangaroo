import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChartsComponent} from './charts.component';
import {ChartsService} from './charts.service';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@NgModule({
  imports: [
    CommonModule,
    NgxChartsModule
  ],
  exports: [
    ChartsComponent
  ],
  providers: [ChartsService],
  declarations: [ChartsComponent]
})
export class ChartsModule {
}
