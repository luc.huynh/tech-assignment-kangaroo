import { Injectable } from '@angular/core';
import { TIME } from '../../constant/time.constant';

@Injectable({
  providedIn: 'root'
})
export class ChartsService {

  constructor() {
  }

  /**
   * @desc Check chart display time no more than one hour
   * @return Chart with length is less than or equal to one hour
   */
  public checkLimitedTimeOfChart(multiLine) {
    multiLine.forEach(line => {
      line.series = this.checkLimitedTime(line.series);
    });

    return multiLine;
  }

  /**
   * @desc Check line chart display time no more than one hour
   * @return Line chart with length is less than or equal to one hour
   */
  public checkLimitedTime(arrayChart) {
    const maxLength = (TIME.LIMITED_TIME / TIME.TIME_TO_RELOAD) + 1;

    if (arrayChart.length > maxLength) {
      arrayChart.slice(arrayChart.length - maxLength);
    }

    return arrayChart;
  }

  /**
   * @desc Convert timestamp to human hour
   * @return Human hour
   */
  public convertTimestamp(unixTimestamp: number) {
    const date = new Date(unixTimestamp * 1000);

    const hours = date.getHours();
    const minutes = '0' + date.getMinutes();
    const seconds = '0' + date.getSeconds();

    return hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
  }

  /**
   * @desc Clear all element of series of line charts
   * @return Line charts with list of series cleared
   */
  public clearMultiLine(multiLine) {
    multiLine.forEach(line => {
      line.series = [];
    });

    return multiLine;
  }
}
