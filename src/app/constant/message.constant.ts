export const MESSAGE = {
  OUT_OF_QUOTA: 'Your quota has been used fully. Quotas reset at midnight EST',
  SOMETHING_WRONG: 'Something went wrong!'
};
