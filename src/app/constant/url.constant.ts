export const URL = {
  QUOTES: 'https://forex.1forge.com/1.0.3/quotes',
  SYMBOLS_GET: 'https://forex.1forge.com/1.0.3/symbols',
  MARKET_STATUS_GET: 'https://forex.1forge.com/1.0.3/market_status',
  QUOTA_GET: 'https://forex.1forge.com/1.0.3/quota'
};
