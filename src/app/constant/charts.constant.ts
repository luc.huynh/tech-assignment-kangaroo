export const CHARTS = {
  X_AXIS_LABEL: 'Time',
  Y_AXIS_LABEL: 'price/ask/bid',
  X_VIEW: 600,
  Y_VIEW: 1100,
  PRICE: 'Price',
  ASK: 'Ask',
  BID: 'Bid',
  PRICE_INDEX: 0,
  ASK_INDEX: 1,
  BID_INDEX: 2
};
