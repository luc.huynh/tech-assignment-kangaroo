export const COLOR = {
  PRICE_LINE: '#5AA454',
  ASK_LINE: '#A10A28',
  BID_LINE: '#C7B42C'
};
